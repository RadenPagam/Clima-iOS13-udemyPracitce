//
//  WeatherApiManager.swift
//  Clima
//
//  Created by Dimas Pagam on 08/08/22.
//  Copyright © 2022 App Brewery. All rights reserved.
//

import Foundation
import CoreLocation

protocol weatherApiManagerDelegate{
    func performUpdateData(weather:WeatherModel)
    func didfailWithError(error:Error)
}
struct WeatherManager{
    
    let url = "https://api.openweathermap.org/data/2.5/weather?units=metric&appid=aa1fb2195a9bbd43d8f171bd71bdeac3"
    var delegate:weatherApiManagerDelegate?
    
    //MARK: - fetch weather data by city name
    func fetchCityData(cityName:String){
        let urlString = "\(url)&q=\(cityName)"
            performRequest(with: urlString)
    }
    
    //MARK: - fetch weather data by city name
    func fetchCurrLoc(_ latitude:CLLocationDegrees,_ longitude:CLLocationDegrees){
        let urlString = "\(url)&lat=\(latitude)&lon=\(longitude)"
        performRequest(with: urlString)
    }
    
    
    func performRequest(with urlString:String){
        //1. create url
        if let url = URL(string: urlString){
            //2.create url session
            let session = URLSession(configuration: .default)
            
            //3. give session tast
            let task = session.dataTask(with: url) { data, response, error in
                if error != nil{
                    self.delegate?.didfailWithError(error: error!)
                    print(error!)
                    return
                }
                
                if let safeData = data{
                    //let dataString = String(data: safeData, encoding: .utf8)
                    if let weather = self.parseJSON(weatherData: safeData){
                        self.delegate?.performUpdateData(weather:weather)
                    }
                }
            }
            
            //4. start task
            task.resume()
        }
    }
    
    func parseJSON(weatherData:Data)-> WeatherModel?{
        let decoder = JSONDecoder()
        do{
            let decodeData = try decoder.decode(WeatherData.self, from: weatherData)
            let id = decodeData.weather[0].id
            let name = decodeData.name
            let temp = decodeData.main.temp
            let weather = WeatherModel(conditionID: id, cityName: name, temperature: temp)
            return weather
            
        }catch{
            delegate?.didfailWithError(error: error)
            return nil
        }
    }
}
