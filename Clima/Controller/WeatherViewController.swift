//
//  ViewController.swift
//  Clima
//
//  Created by Angela Yu on 01/09/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController{

    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    var weatherManager = WeatherManager()
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //MARK: - Core Location
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        //MARK: - delegate of weather
        searchTextField.delegate = self
        weatherManager.delegate = self
        
    }

    @IBAction func searchPressed(_ sender: UIButton) {
        
        print(searchTextField.text ?? "No result")
        searchTextField.endEditing(true)
    
    }
    
    @IBAction func locationPressed(_ sender: UIButton){
        locationManager.requestLocation()
        
    }
    
  
}

//MARK: - CoreLocation
extension WeatherViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            locationManager.stopUpdatingLocation()
            DispatchQueue.main.async {
                let lat = location.coordinate.latitude
                let lon = location.coordinate.longitude
                self.weatherManager.fetchCurrLoc(lat, lon)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("CLLocation error: \(error)")
    }
}
//MARK: - weather api delegate
extension WeatherViewController:weatherApiManagerDelegate{
    //delegate
    func performUpdateData(weather: WeatherModel) {
        print("transported data: \( weather.conditionName)")
        DispatchQueue.main.async {
            self.cityLabel.text = weather.cityName
            self.temperatureLabel.text = weather.temperatureString
            self.conditionImageView.image = UIImage(systemName:  weather.conditionName)
        }
       
    }
    
    func didfailWithError(error: Error) {
        print(error)
        DispatchQueue.main.async {
            self.cityLabel.text = "Error no data"
            self.temperatureLabel.text = "Error no data"
            
        }
    }
   
}

//MARK: - Textfield delegate

extension WeatherViewController:UITextFieldDelegate{
    //textfield end editing
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != ""{
            return true
        }else{
            textField.placeholder = "Type City Name"
            return false
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let city = searchTextField.text{
            weatherManager.fetchCityData(cityName: city)
            
        }
        
        
        searchTextField.text = ""
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(searchTextField.text ?? "No result")
        searchTextField.endEditing(true)
        return true
    }
}
